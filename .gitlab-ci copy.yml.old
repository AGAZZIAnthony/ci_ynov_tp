images: ubuntu:20.04
stages:
  - install
  - lint
  - build
  - test
  - deploy

cache:
  key: node_modules
  paths:
    - node_modules/

install: 
  stage: install
  image: node:14.13.1
  cache:
    key: node_modules
    paths:
      - node_modules/
    policy: push
  script:
    - npm install --silent

lint: 
  stage: lint
  image: node:14.13.1
  cache:
    key: node_modules
    paths:
      - node_modules/
    policy: push
  script:
    - npm run eslint

build:test:
  stage: test
  image: node:14.13.1
  cache:
    key: node_modules
    paths:
      - node_modules/
    policy: pull
  script:
    - npm run test
  artifacts:
    paths:
      - test-report.html
    expire_in: 1 week
    when: 
      - on_success
  only:
    - develop
  
publish:test:
  stage: test
  needs: build:test
  image: kroniak/ssh-client
  script:
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - ssh $SSH_USER@$SSH_SERVER_IP -p $SSH_PORT "mkdir -p $SSH_DIR/maitre/test-report/"
    - scp -r -P $SSH_PORT ./test-report.html $SSH_USER@$SSH_SERVER_IP:$SSH_DIR/maitre/test-report/
    - mv ./test-report.html ./report_${CI_COMMIT_REF_SLUG}_${CI_JOB}.html
  only:
    - develop
  allow_failure: true

slack:message:
  variables:
    SLACK_WEBHOOK: https://hooks.slack.com/services/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
    SLACK_CHANNEL: maitre
  needs: 
    - build:test
  when: on_failure
  script:
    - apt-get update && apt-get install --yes curl
    - echo '{"channel":"'$SLACK_CHANNEL'", "username":"Maître", "text":"http://51.75.126.124:30080/maitre/test-report/report_'${CI_COMMIT_REF_SLUG}'_'${CI_JOB}'"}' > data.json
    - curl -X POST -d @data.json $SLACK_WEBHOOK

build:
  stage: build
  cache:
    key: node_modules
    paths:
      - node_modules/
    policy: push
  script:
    - npm run build

publish:master:
  stage: publish
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  script:
    - docker build -f Dockerfile -t ynov_ci:latest .  
  only:
    - master

publish:develop:
  stage: publish
  image: docker:19.03.12
  services:
    - docker:19.03.12-dind
  script:
    - docker build -f Dockerfile.dev -t ynov_ci:develop .
  only:
    - develop

schedules:sonarqube:
  images: sonarqube:lts
  only:
    - schedules
  script:
    - sonar-scanner -Dsonar.projectKey=maitre -Dsonar.sources=. -Dsonar.host.url=https://sonarqube.devzone.fr -Dsonar.login=3d2eeddcc1b1f21c1c622cf37c303404d7394793