import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import * as cors from 'cors';
import { configureRouter } from './configure';
import { addStartTime, expressMetricsMiddleware } from './utils/express-metrics.middleware';
import { handleErrorMiddleware } from './utils/handle-error.middleware';
import * as uuid from 'uuid/v4';
import { configureLogger, defaultWinstonLoggerOptions } from './utils/logger';
import { initContextMiddleware } from './utils/init-context.middleware';
import { pagingMiddleware } from './utils/paging.middleware';

configureLogger('mainApp', defaultWinstonLoggerOptions);

export class App {
    private readonly _uuid: string;

    private _appName: string;
    private _isReady = false;

    constructor(params: any) {
        this._expressApp = express();

        this.appName = params.appName;

        if (params.configuration) {
            if (this.appName in params.configuration) {
                this.configuration = params.configuration[this.appName];
            }
        }

        this._expressApp.set('port', params.port || process.env.PORT || 3000);
        this._expressApp.set('env', params.env || process.env.NODE_ENV || 'development');
        this._uuid = uuid();
    }

    private _token: string;

    get token(): string {
        return this._token;
    }

    get isReady(): boolean {
        return this._isReady;
    }


    get appName(): string {
        return this._appName;
    }

    set appName(value: string) {
        this._appName = value;
    }

    get uuid(): string {
        return this._uuid;
    }

    private _expressApp: express.Application;

    get expressApp(): express.Application {
        return this._expressApp;
    }

    set expressApp(value: express.Application) {
        this._expressApp = value;
    }

    private _port: number;

    get port(): number {
        return this._port;
    }

    set port(value: number) {
        this._port = value;
    }

    private _configuration?: { [key: string]: any };

    get configuration(): { [p: string]: any } {
        return this._configuration;
    }

    set configuration(value: { [p: string]: any }) {
        this._configuration = value;
    }

    async registerAppRouters() {
        const appRouters: express.Router[] = configureRouter(this.configuration);
        // Mount public router to /
        this.expressApp.use('/', appRouters[0]);

        if (appRouters[1]) {
            // Mount private router to /_appName
            this.expressApp.use(`/_${this.appName}`, appRouters[1]);
        }
    }

    applyExpressMiddlewaresRouter(): void {
        this.expressApp.use(initContextMiddleware);
        this.expressApp.use(addStartTime);
        this.expressApp.use(bodyParser.json());
        this.expressApp.use(bodyParser.urlencoded({ extended: true }));
        this.expressApp.use(helmet());
        this.expressApp.use(cors());
        this.expressApp.use(expressMetricsMiddleware);
        this.expressApp.use(pagingMiddleware);
    }

    async bootstrap() {
        this.applyExpressMiddlewaresRouter();
        await this.registerAppRouters();
        this.expressApp.use(handleErrorMiddleware); // error handler middleware should be put after router
    }

}
